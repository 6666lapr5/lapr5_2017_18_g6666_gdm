﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Semente.Migrations
{
    public partial class Iteracao22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PosologiaBula_Apresentacao_ApresentacaoId",
                table: "PosologiaBula");

            migrationBuilder.AlterColumn<int>(
                name: "ApresentacaoId",
                table: "PosologiaBula",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PosologiaBula_Apresentacao_ApresentacaoId",
                table: "PosologiaBula",
                column: "ApresentacaoId",
                principalTable: "Apresentacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PosologiaBula_Apresentacao_ApresentacaoId",
                table: "PosologiaBula");

            migrationBuilder.AlterColumn<int>(
                name: "ApresentacaoId",
                table: "PosologiaBula",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_PosologiaBula_Apresentacao_ApresentacaoId",
                table: "PosologiaBula",
                column: "ApresentacaoId",
                principalTable: "Apresentacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
