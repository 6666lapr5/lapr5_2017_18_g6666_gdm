﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Semente.Migrations
{
    public partial class Iteracao1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Farmaco",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Farmaco", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PosologiaPrescrita",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Dias = table.Column<int>(type: "int", nullable: false),
                    Intervalo_horas = table.Column<int>(type: "int", nullable: false),
                    Quantidade = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PosologiaPrescrita", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Apresentacao",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Dosagem = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FarmacoId = table.Column<int>(type: "int", nullable: false),
                    FormaFarmaceutica = table.Column<int>(type: "int", nullable: false),
                    MedicamentoId = table.Column<int>(type: "int", nullable: true),
                    TamanhoEmbalagem = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Apresentacao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Apresentacao_Farmaco_FarmacoId",
                        column: x => x.FarmacoId,
                        principalTable: "Farmaco",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Apresentacao_Medicamento_MedicamentoId",
                        column: x => x.MedicamentoId,
                        principalTable: "Medicamento",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PosologiaBula",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApresentacaoId = table.Column<int>(type: "int", nullable: true),
                    Dias = table.Column<int>(type: "int", nullable: false),
                    Intervalo_horas = table.Column<int>(type: "int", nullable: false),
                    Peso_max = table.Column<int>(type: "int", nullable: false),
                    Peso_min = table.Column<int>(type: "int", nullable: false),
                    Quantidade = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PosologiaBula", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PosologiaBula_Apresentacao_ApresentacaoId",
                        column: x => x.ApresentacaoId,
                        principalTable: "Apresentacao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Apresentacao_FarmacoId",
                table: "Apresentacao",
                column: "FarmacoId");

            migrationBuilder.CreateIndex(
                name: "IX_Apresentacao_MedicamentoId",
                table: "Apresentacao",
                column: "MedicamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_PosologiaBula_ApresentacaoId",
                table: "PosologiaBula",
                column: "ApresentacaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PosologiaBula");

            migrationBuilder.DropTable(
                name: "PosologiaPrescrita");

            migrationBuilder.DropTable(
                name: "Apresentacao");

            migrationBuilder.DropTable(
                name: "Farmaco");
        }
    }
}
