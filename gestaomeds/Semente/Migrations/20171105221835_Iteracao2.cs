﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Semente.Migrations
{
    public partial class Iteracao2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoId",
                table: "Apresentacao");

            migrationBuilder.DropTable(
                name: "PosologiaPrescrita");

            migrationBuilder.AlterColumn<int>(
                name: "MedicamentoId",
                table: "Apresentacao",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoId",
                table: "Apresentacao",
                column: "MedicamentoId",
                principalTable: "Medicamento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoId",
                table: "Apresentacao");

            migrationBuilder.AlterColumn<int>(
                name: "MedicamentoId",
                table: "Apresentacao",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "PosologiaPrescrita",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Dias = table.Column<int>(nullable: false),
                    Intervalo_horas = table.Column<int>(nullable: false),
                    Quantidade = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PosologiaPrescrita", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoId",
                table: "Apresentacao",
                column: "MedicamentoId",
                principalTable: "Medicamento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
