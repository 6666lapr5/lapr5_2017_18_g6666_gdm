﻿using Semente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.DTO
{
    public class ApresentacaoDetalheDTO
    {
        public int Id { get; set; }
        public string Dosagem { get; set; }
        public int TamanhoEmbalagem { get; set; }
        public string FormaFarmaceutica { get; set; }
    }
}
