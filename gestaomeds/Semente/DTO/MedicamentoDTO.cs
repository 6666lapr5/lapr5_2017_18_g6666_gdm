﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.DTO
{
    public class MedicamentoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        
    }
}
