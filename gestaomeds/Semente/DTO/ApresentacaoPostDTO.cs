﻿using Semente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.DTO
{
    public class ApresentacaoPostDTO
    {
        public string Dosagem { get; set; }
        public int TamanhoEmbalagem { get; set; }
        public FormaFarmaceuticaEnum.formaFarmaceutica FormaFarmaceutica { get; set; }
        public int FarmacoId { get; set; }
        public int MedicamentoId { get; set; }
    }
}
