﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class PosologiaBula
    {
        public int Id { get; set; }
        public int Quantidade { get; set; }
        public int Dias { get; set; }
        public int Intervalo_horas { get; set; }
        public int Peso_min { get; set; }
        public int Peso_max { get; set; }
        public Apresentacao Apresentacao { get; set; }
        public int ApresentacaoId { get; set; }
    }
}
