﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class Apresentacao
    {

        public int Id { get; set; }
        public string Dosagem { get; set; }
        public int TamanhoEmbalagem { get; set; }
        public FormaFarmaceuticaEnum.formaFarmaceutica FormaFarmaceutica { get; set; }
        //Foreign Key
        public int FarmacoId { get; set; }
        public Farmaco Farmaco { get; set; }
        public int MedicamentoId { get; set; }
        public Medicamento Medicamento { get; set; }

    }
}
