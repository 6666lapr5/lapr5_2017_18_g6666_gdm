﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class FormaFarmaceuticaEnum
    {
        public enum formaFarmaceutica
        {
            Comprimido,
            Cápsula,
            Drageia,
            Pó,
            Granulado,
            Pomada,
            Creme,
            Gel,
            Pasta,
            Solução,
            Gotas,
            Xarope,
            Suspensão,
            Elixir
        }
    }
}
