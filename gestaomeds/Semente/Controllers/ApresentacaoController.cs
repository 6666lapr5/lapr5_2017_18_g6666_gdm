﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using Semente.DTO;
using Microsoft.AspNetCore.Authorization;

namespace Semente.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Apresentacao")]
    public class ApresentacaoController : Controller
    {
        private readonly SementeContext _context;

        public ApresentacaoController(SementeContext context)
        {
            _context = context;
        }

        // GET: api/Apresentacao
        [HttpGet]
        public IEnumerable<ApresentacaoDetalheDTO> GetApresentacao()
        {
            List<ApresentacaoDetalheDTO> apresentacoes = new List<ApresentacaoDetalheDTO>();
            foreach (Apresentacao a in _context.Apresentacao)
            {
                ApresentacaoDetalheDTO aDTO = new ApresentacaoDetalheDTO()
                {
                    Id = a.Id,
                    Dosagem = a.Dosagem,
                    TamanhoEmbalagem = a.TamanhoEmbalagem,
                    FormaFarmaceutica = a.FormaFarmaceutica.ToString()
                };
                apresentacoes.Add(aDTO);
            }
            return apresentacoes;
        }

        // GET: api/Apresentacao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApresentacao([FromRoute] int id)
        {

            var apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(a => a.Id == id);

            ApresentacaoDetalheDTO aDTO = new ApresentacaoDetalheDTO()
            {
                Id = apresentacao.Id,
                Dosagem = apresentacao.Dosagem,
                TamanhoEmbalagem = apresentacao.TamanhoEmbalagem,
                FormaFarmaceutica = apresentacao.FormaFarmaceutica.ToString()
            };

            if (apresentacao == null)
            {
                return NotFound();
            }

            return Ok(aDTO);
        }

        // PUT: api/Apresentacao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApresentacao([FromRoute] int id, [FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacao.Id)
            {
                return BadRequest();
            }

            _context.Entry(apresentacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApresentacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Apresentacao
        [HttpPost]
        public async Task<IActionResult> PostApresentacao([FromBody] ApresentacaoPostDTO apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologiasBulaApresentacao = new List<PosologiaBula>();

            var posologiasBula = _context.PosologiaBula;

            var farmacoApresentacao = await _context.Farmaco.SingleOrDefaultAsync(f => f.Id == apresentacao.FarmacoId);
            var medicamentoApresentacao = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == apresentacao.MedicamentoId);

            Apresentacao a = new Apresentacao
            {
                Dosagem = apresentacao.Dosagem,
                TamanhoEmbalagem = apresentacao.TamanhoEmbalagem,
                FormaFarmaceutica = apresentacao.FormaFarmaceutica,
                FarmacoId = apresentacao.FarmacoId,
                MedicamentoId = apresentacao.MedicamentoId,
                Medicamento = medicamentoApresentacao,
                Farmaco = farmacoApresentacao,
            };

            _context.Apresentacao.Add(a);
            await _context.SaveChangesAsync();

            ApresentacaoDetalheDTO aDTO = new ApresentacaoDetalheDTO()
            {
                Id = a.Id,
                Dosagem = a.Dosagem,
                TamanhoEmbalagem = a.TamanhoEmbalagem,
                FormaFarmaceutica = a.FormaFarmaceutica.ToString()
            };

            return CreatedAtAction("GetApresentacao", new { id = a.Id }, aDTO);
        }

        // DELETE: api/Apresentacao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(m => m.Id == id);
            if (apresentacao == null)
            {
                return NotFound();
            }

            _context.Apresentacao.Remove(apresentacao);
            await _context.SaveChangesAsync();

            return Ok(apresentacao);
        }

        private bool ApresentacaoExists(int id)
        {
            return _context.Apresentacao.Any(e => e.Id == id);
        }
    }
}