﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using Semente.DTO;
using Microsoft.AspNetCore.Authorization;

namespace Semente.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/PosologiasBula")]
    public class PosologiasBulaController : Controller
    {
        private readonly SementeContext _context;

        public PosologiasBulaController(SementeContext context)
        {
            _context = context;
        }

        // GET: api/PosologiasBula
        [HttpGet]
        public IQueryable<PosologiaBulaDTO> GetPosologiaBula()
        {
            var posologiasBula = from pb in _context.PosologiaBula
                                 select new PosologiaBulaDTO()
                                 {
                                     Id = pb.Id,
                                     Quantidade = pb.Quantidade,
                                     Dias = pb.Dias,
                                     Intervalo_horas = pb.Intervalo_horas,
                                     Peso_min = pb.Peso_min,
                                     Peso_max = pb.Peso_max,
                                     ApresentacaoId = pb.ApresentacaoId
                               };
            return posologiasBula;
        }

        // GET: api/PosologiasBula/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPosologiaBula([FromRoute] int id)
        {
            var posologiasBula = await _context.PosologiaBula.Select(pb => new PosologiaBulaDTO()
            {
                Id = pb.Id,
                Quantidade = pb.Quantidade,
                Dias = pb.Dias,
                Intervalo_horas = pb.Intervalo_horas,
                Peso_min = pb.Peso_min,
                Peso_max = pb.Peso_max,
                ApresentacaoId = pb.ApresentacaoId
            }).SingleOrDefaultAsync(pb => pb.Id == id);

            if (posologiasBula == null)
            {
                return NotFound();
            }

            return Ok(posologiasBula);
        }

        // PUT: api/PosologiasBula/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPosologiaBula([FromRoute] int id, [FromBody] PosologiaBula posologiaBula)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != posologiaBula.Id)
            {
                return BadRequest();
            }

            _context.Entry(posologiaBula).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PosologiaBulaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PosologiasBula
        [HttpPost]
        public async Task<IActionResult> PostPosologiaBula([FromBody] PosologiaBula posologiaBula)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PosologiaBula.Add(posologiaBula);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPosologiaBula", new { id = posologiaBula.Id }, posologiaBula);
        }

        // DELETE: api/PosologiasBula/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePosologiaBula([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologiaBula = await _context.PosologiaBula.SingleOrDefaultAsync(m => m.Id == id);
            if (posologiaBula == null)
            {
                return NotFound();
            }

            _context.PosologiaBula.Remove(posologiaBula);
            await _context.SaveChangesAsync();

            return Ok(posologiaBula);
        }

        private bool PosologiaBulaExists(int id)
        {
            return _context.PosologiaBula.Any(e => e.Id == id);
        }
    }
}