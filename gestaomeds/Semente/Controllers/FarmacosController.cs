﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using Semente.DTO;
using Microsoft.AspNetCore.Authorization;

namespace Semente.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Farmacos")]
    public class FarmacosController : Controller
    {
        private readonly SementeContext _context;

        public FarmacosController(SementeContext context)
        {
            _context = context;
        }

        // GET: api/Farmacos
        [HttpGet]
        public IQueryable<FarmacoDTO> GetFarmaco()
        {
            var farmacos = from f in _context.Farmaco
                           select new FarmacoDTO()
                           {
                               Id = f.Id,
                               Nome = f.Nome
                           };
            return farmacos;
        }

        // GET: api/Farmacos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.Select(f => new FarmacoDTO()
            {
                Id = f.Id,
                Nome = f.Nome
            }).SingleOrDefaultAsync(f => f.Id == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }

        // GET: api/Farmacos/nome=Paracetamol
        [HttpGet("nome={nome}")]
        public async Task<IActionResult> GetFarmaco([FromRoute] String nome)
        {
            var farmaco = await _context.Farmaco.Select(f => new FarmacoDTO()
            {
                Id = f.Id,
                Nome = f.Nome
            }).SingleOrDefaultAsync(f => f.Nome.ToLower().Contains(nome.ToLower()));

            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }

        // GET: api/Farmacos/5/Medicamentos
        [HttpGet("{id}/Medicamentos")]
        public async Task<IActionResult> GetFarmacoMedicamentos([FromRoute] int id)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(f => f.Id == id);

            var apresentacoes = _context.Apresentacao;

            var medicamentos = _context.Medicamento;

            List<Apresentacao> apresentacoesDeFarmaco = new List<Apresentacao>();
            foreach (Apresentacao a in apresentacoes)
            {
                if (a.FarmacoId == id)
                {
                    apresentacoesDeFarmaco.Add(a);
                }
            }
            List<MedicamentoDTO> medFinal = new List<MedicamentoDTO>();

            foreach (Medicamento m in medicamentos)
            {
                foreach (Apresentacao a in apresentacoesDeFarmaco)
                {
                    if (m.Id == a.MedicamentoId)
                    {
                        MedicamentoDTO med = new MedicamentoDTO()
                        {
                            Id = m.Id,
                            Nome = m.Nome
                        };
                        medFinal.Add(med);
                    }
                }

            }
            return Ok(medFinal);

        }

        // GET: api/Farmacos/5/Posologias
        [HttpGet("{id}/Posologias")]
        public async Task<IActionResult> GetFarmacoPosologia([FromRoute] int id)
        {

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(f => f.Id == id);

            var apresentacoes = _context.Apresentacao;
            var posologias = _context.PosologiaBula;

            List<PosologiaBulaDTO> posologiasBula = new List<PosologiaBulaDTO>();

            foreach (Apresentacao a in apresentacoes)
            {
                if (a.FarmacoId == id)
                {
                    foreach (PosologiaBula pb in posologias)
                    {
                        if (pb.ApresentacaoId == a.Id)
                        {
                            posologiasBula.Add(new PosologiaBulaDTO()
                            {
                                Id = pb.Id,
                                Quantidade = pb.Quantidade,
                                Dias = pb.Dias,
                                Intervalo_horas = pb.Intervalo_horas,
                                Peso_min = pb.Peso_min,
                                Peso_max = pb.Peso_max
                            });
                        }
                    }
                }
            }


            return Ok(posologiasBula);
        }

        // GET: api/Farmacos/5/Apresentacoes
        [HttpGet("{id}/Apresentacoes")]
        public async Task<IActionResult> GetFarmacoApresentacoes([FromRoute] int id)
        {

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(f => f.Id == id);

            var apresentacoes = _context.Apresentacao;

            List<int> apresentacoesIds = new List<int>();

            foreach (Apresentacao a in apresentacoes)
            {
                if (a.FarmacoId == id)
                {
                    apresentacoesIds.Add(a.Id);
                }
            }

            return Ok(apresentacoesIds);

        }


        // PUT: api/Farmacos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFarmaco([FromRoute] int id, [FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farmaco.Id)
            {
                return BadRequest();
            }

            _context.Entry(farmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FarmacoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Farmacos
        [HttpPost]
        public async Task<IActionResult> PostFarmaco([FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Farmaco.Add(farmaco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFarmaco", new { id = farmaco.Id }, farmaco);
        }

        // DELETE: api/Farmacos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            _context.Farmaco.Remove(farmaco);
            await _context.SaveChangesAsync();

            return Ok(farmaco);
        }

        private bool FarmacoExists(int id)
        {
            return _context.Farmaco.Any(e => e.Id == id);
        }
    }
}