﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using Semente.DTO;
using Microsoft.AspNetCore.Authorization;

namespace Semente.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Medicamentos")]
    public class MedicamentosController : Controller
    {
        private readonly SementeContext _context;

        public MedicamentosController(SementeContext context)
        {
            _context = context;

        }

        // GET: api/Medicamentos
        [HttpGet]
        public IQueryable<MedicamentoDTO> GetMedicamento()
        {
            var medicamentos = from m in _context.Medicamento
                               select new MedicamentoDTO()
                               {
                                   Id = m.Id,
                                   Nome = m.Nome
                               };
            return medicamentos;
        }

        // GET: api/Medicamentos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedicamento([FromRoute] int id)
        {

            var medicamento = await _context.Medicamento.Select(m => new MedicamentoDTO()
            {
                Id = m.Id,
                Nome = m.Nome
            }).SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            return Ok(medicamento);
        }

        // GET: api/Medicamentos/5/Apresentacoes
        [HttpGet("{id}/Apresentacoes")]
        public async Task<IActionResult> GetMedicamentoApresentacoes([FromRoute] int id)
        {

            var medicamento = await _context.Medicamento.Select(m => new Medicamento()
            {
                Id = m.Id,
                Nome = m.Nome,
            }).SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            var apresentacoes = _context.Apresentacao;

            var apresentacoesDTO = new List<int>();
            foreach (Apresentacao a in apresentacoes)
            {
                if (a.MedicamentoId == id)
                {
                    apresentacoesDTO.Add(a.Id);
                }
            }

            return Ok(apresentacoesDTO);
        }

        // GET: api/Medicamentos/5/Apresentacoes
        [HttpGet("{id}/Posologias")]
        public async Task<IActionResult> GetMedicamentoPosologias([FromRoute] int id)
        {

            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            var apresentacoes = _context.Apresentacao;
            var posologias = _context.PosologiaBula;

            List<PosologiaBulaDTO> posologiasBula = new List<PosologiaBulaDTO>();

            foreach (Apresentacao a in apresentacoes)
            {
                if (id == a.MedicamentoId)
                {
                    foreach (PosologiaBula pb in posologias)
                    {
                        if (pb.ApresentacaoId == a.Id)
                        {
                            posologiasBula.Add(new PosologiaBulaDTO()
                            {
                                Id = pb.Id,
                                Quantidade = pb.Quantidade,
                                Dias = pb.Dias,
                                Intervalo_horas = pb.Intervalo_horas,
                                Peso_min = pb.Peso_min,
                                Peso_max = pb.Peso_max,
                                ApresentacaoId = pb.ApresentacaoId
                            });
                        }
                    }
                }
            }
            return Ok(posologiasBula);
        }

        //GET: api/Medicamentos/brufen
        [HttpGet("nome={nome}")]
        public async Task<IActionResult> GetMedicamento([FromRoute] string nome)
        {
            var medicamento = await _context.Medicamento.Select(m =>
            new MedicamentoDTO()
            {
                Id = m.Id,
                Nome = m.Nome
            }).SingleOrDefaultAsync(m => m.Nome.ToLower().Contains(nome.ToLower()));
            return Ok(medicamento);
        }

        // PUT: api/Medicamentos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicamento([FromRoute] int id, [FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicamento.Id)
            {
                return BadRequest();
            }

            _context.Entry(medicamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicamentos
        [HttpPost]
        public async Task<IActionResult> PostMedicamento([FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacoes = _context.Apresentacao;

            Medicamento m = new Medicamento
            {
                Nome = medicamento.Nome
            };

            _context.Medicamento.Add(m);
            await _context.SaveChangesAsync();

            MedicamentoDTO mDTO = new MedicamentoDTO()
            {
                Id = m.Id,
                Nome = m.Nome
            };

            return CreatedAtAction("GetMedicamento", new { id = m.Id }, mDTO);
        }

        // DELETE: api/Medicamentos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);
            if (medicamento == null)
            {
                return NotFound();
            }

            _context.Medicamento.Remove(medicamento);
            await _context.SaveChangesAsync();

            return Ok(medicamento);
        }

        private bool MedicamentoExists(int id)
        {
            return _context.Medicamento.Any(e => e.Id == id);
        }
    }
}