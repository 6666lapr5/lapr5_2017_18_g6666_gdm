﻿using Semente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Data
{
    public class DbInitializer
    {
        public static void Initialize(SementeContext context)
        {
            context.Database.EnsureCreated();
            if (context.Medicamento.Any() && context.Farmaco.Any() && context.Apresentacao.Any() && context.PosologiaBula.Any())
            {
                return;     //DB has already been seeded
            }

            var farmacos = new Farmaco[]
            {
                new Farmaco{Nome = "Paracetamol"},
                new Farmaco{Nome = "Ibuprofeno"},
                new Farmaco{Nome = "Amoxicilina"},
                new Farmaco{Nome = "Aciclovir"},
                new Farmaco{Nome = "Ioratadina"}
            };

            foreach (Farmaco f in farmacos)
            {
                context.Farmaco.Add(f);
            }
            context.SaveChanges();

            List<Farmaco> fl = new List<Farmaco>();

            var farmacosDB = context.Farmaco;
            foreach (Farmaco f in farmacosDB)
            {
                fl.Add(f);
            }

            var medicamentos = new Medicamento[]
            {
                new Medicamento{Nome="Ben-u-ron"},
                new Medicamento{Nome="Maxilase"},
                new Medicamento{Nome="Strepfen"},
                new Medicamento{Nome="Vibrosil"},
                new Medicamento{Nome="Gaviscon"},
                new Medicamento{Nome="Cêgripe"},
                new Medicamento{Nome="Brufen"},
                new Medicamento{Nome="Ilvico"},
                new Medicamento{Nome="Claridon"},
                new Medicamento{Nome="Zovirax"}
            };

            foreach (Medicamento m in medicamentos)
            {
                context.Medicamento.Add(m);
            }
            context.SaveChanges();

            List<Medicamento> ml = new List<Medicamento>();

            var medicamentosDB = context.Medicamento;
            foreach (Medicamento m in medicamentosDB)
            {
                ml.Add(m);
            }

            var apresentacoes = new Apresentacao[]
            {
                new Apresentacao{
                Dosagem ="10mg", TamanhoEmbalagem=32, FormaFarmaceutica=FormaFarmaceuticaEnum.formaFarmaceutica.Comprimido, FarmacoId = fl[0].Id, Farmaco = fl[0], MedicamentoId = ml[0].Id,Medicamento = ml[0]},
                new Apresentacao{
                Dosagem ="400mg", TamanhoEmbalagem=20, FormaFarmaceutica=FormaFarmaceuticaEnum.formaFarmaceutica.Comprimido, FarmacoId = fl[1].Id, Farmaco = fl[1], MedicamentoId = ml[0].Id,Medicamento = ml[0]},
                new Apresentacao{
                Dosagem ="250mg", TamanhoEmbalagem=21, FormaFarmaceutica=FormaFarmaceuticaEnum.formaFarmaceutica.Cápsula, FarmacoId = fl[2].Id, Farmaco = fl[2], MedicamentoId = ml[2].Id,Medicamento = ml[2]},
                new Apresentacao{
                Dosagem ="50mg/g", TamanhoEmbalagem=1, FormaFarmaceutica=FormaFarmaceuticaEnum.formaFarmaceutica.Creme, FarmacoId = fl[3].Id, Farmaco = fl[3], MedicamentoId = ml[3].Id,Medicamento = ml[3]},
                new Apresentacao{
                Dosagem ="10mg", TamanhoEmbalagem=20, FormaFarmaceutica=FormaFarmaceuticaEnum.formaFarmaceutica.Comprimido, FarmacoId = fl[4].Id, Farmaco = fl[4], MedicamentoId = ml[0].Id,Medicamento = ml[0]},
                new Apresentacao{
                Dosagem ="150mg", TamanhoEmbalagem=15, FormaFarmaceutica=FormaFarmaceuticaEnum.formaFarmaceutica.Comprimido, FarmacoId = fl[0].Id, Farmaco = fl[0], MedicamentoId = ml[4].Id,Medicamento = ml[4]},
            };

            foreach (Apresentacao a in apresentacoes)
            {
                context.Apresentacao.Add(a);
            }

            context.SaveChanges();

            List<Apresentacao> al = new List<Apresentacao>();

            var apresentacoesDB = context.Apresentacao;

            foreach (Apresentacao a in apresentacoesDB)
            {
                al.Add(a);
            }

            var posologiasBula = new PosologiaBula[]
            {
                new PosologiaBula{Quantidade=32, Dias=7, Intervalo_horas=8, Peso_min=50, Peso_max=90, Apresentacao = al[0], ApresentacaoId = al[0].Id},
                new PosologiaBula{Quantidade=3, Dias=3, Intervalo_horas=24, Peso_min=50, Peso_max=90, Apresentacao = al[1], ApresentacaoId = al[1].Id},
                new PosologiaBula{Quantidade=12, Dias=5, Intervalo_horas=8, Peso_min=50, Peso_max=90, Apresentacao = al[2], ApresentacaoId = al[2].Id},
                new PosologiaBula{Quantidade=22, Dias=5, Intervalo_horas=12, Peso_min=70, Peso_max=120, Apresentacao = al[3], ApresentacaoId = al[3].Id},
                new PosologiaBula{Quantidade=1, Dias=4, Intervalo_horas=4, Peso_min=40, Peso_max=120, Apresentacao = al[0], ApresentacaoId = al[0].Id},
                new PosologiaBula{Quantidade=24, Dias=7, Intervalo_horas=8, Peso_min=50, Peso_max=90, Apresentacao = al[5], ApresentacaoId = al[5].Id},
                new PosologiaBula{Quantidade=24, Dias=7, Intervalo_horas=8, Peso_min=50, Peso_max=90, Apresentacao = al[4], ApresentacaoId = al[4].Id},
            };

            foreach (PosologiaBula pb in posologiasBula)
            {
                context.PosologiaBula.Add(pb);
            }
            context.SaveChanges();

        }
    }
}
